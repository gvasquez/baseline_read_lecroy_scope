#!/usr/bin/env python
### Rev1: added dead channel detection function

from __future__ import absolute_import
from __future__ import unicode_literals
import os

from tekscope_utils import *
from vmm_mo_acq import *
from target_info import *
from xmlmodifier import *
from sendpacket import SendPacket

xm=XmlModifier()
sp=SendPacket()

def baseline_scan(scope,file_name,**kwargs):

	scope_chan=None
	if kwargs.get('scope_channel') is None: scope_chan=1
	else:   scope_chan=kwargs.get('scope_channel')

	#VMM baseline scan
	f=open(file_name,'a')
	f.write("==========Channel Baseline (PT=50ns,Gain=1mV/fC)=====\n")
	f.write("channel\tbaseline(mV)\n")
	f.write("channel\tstlc=0\tstlc=1\n")
	for i in range (64):
		f.write(str(i))
		for j in range (2):
			time.sleep(0.1)
			modlist=[['global1','scmx',1],['global1','smch',i],['global1','stlc',j],['ch'+str(i),'smx',0]]
			xm.vmm_mod('vmm.xml',modlist)
			sp.send('vmm','CfgASIC',target)
			time.sleep(2.0)
			baseline = measure_dclevel(scope,scope_channel=scope_chan,meas_id=1,scale=0.1)
			print '=====channel:',str(i),'baseline (stlc=',j,"):",baseline
			f.write('\t'+str(baseline*1000))
			if j==1: f.write("\n")

def pulser_scan(scope,file_name,**kwargs):

	scope_chan=None
	if kwargs.get('scope_channel') is None: scope_chan=2
	else:   scope_chan=kwargs.get('scope_channel')

	f=open(file_name,'a')
	f.write("===========Pulser DAC=========\n")
	f.write("DAC_Cnt"+"\t"+"Amplitude (mV)\n")
	for i in range (1,51): 
		modlist=[['global1','scmx',0],['global1','smch',1],['global1','sdp',i*20]]
		xm.vmm_mod('vmm.xml',modlist)
		sp.send('vmm','CfgASIC',target)
		if i<=3:
			yscale=0.01
			trig_lvl=0.01
		elif i>3 and i<=10:
			yscale=0.02
			trig_lvl=0.02
		elif i>10 and i<=20:
			yscale=0.04
			trig_lvl=0.02
		elif i>20 and i<=30:
			yscale=0.08
			trig_lvl=0.05
		elif i>30 and i<=40:
			yscale=0.16
			trig_lvl=0.08
		else:
			yscale=0.16 
			trig_lvl=0.1

		pulser_amp = measure_vmm_pulser(scope,scope_channel=scope_chan,meas_id=1,scale=yscale,trig_level=trig_lvl)
		print '=====pulser DAC:',str(i*20),'amplitude',pulser_amp
		f.write(str(i*20)+"\t"+str(pulser_amp*1000)+'\n')
	f.close()

def global_threshold_scan(scope,file_name,**kwargs):

	# threshold measurement accuracy with scope: +-2mV
	# to speed up this calibration: 
	#	- increase step to 20 DAC, 
	#	- stop scan above 500 DAC cnt (high threshold not useful in real life)
	step=20

	scope_chan=None
	if kwargs.get('scope_channel') is None: scope_chan=1
	else:   scope_chan=kwargs.get('scope_channel')

	f=open(file_name,'a')
	f.write("===========Global Threshold=========\n")
	f.write("DAC Count"+"\t"+"Threshold (mV)\n")
	for i in range (1,51):
		print "==========DAC",str(i*step)
		modlist=[['global1','scmx',0],['global1','smch',2],['global1','sdt',i*step]]
		xm.vmm_mod('vmm.xml',modlist)
		sp.send('vmm','CfgASIC',target)

		#taking scope measurement on VMM analog output
		thr_level=measure_dclevel(scope,scope_channel=scope_chan,meas_id=1,scale=0.1)
		time.sleep(0.5)
		#print "threshold level is",thr_level
		f.write(str(i*step)+"\t"+str(thr_level*1000)+'\n')
	f.close()

def channel_threshold_scan(scope,file_name,**kwargs):

	scope_chan=None
	if kwargs.get('scope_channel') is None: scope_chan=1
	else:   scope_chan=kwargs.get('scope_channel')

	#VMM channel trimmed threshold_scan
	f=open(file_name,'a')
	f.write("======Channel Trimmed Threshold Global ThrDAC=210 =====\n")
	f.write("Channel"+"\t\t\t"+"Threshold (mV)\n")
	f.write("       "+"Trim_Bit\t"+"0 -> 31\n")
	for i in range (64):
		f.write(str(i))
		for j in range (32):
			modlist=[['global1','scmx',1],['global1','smch',i],['global1','sdt',210],['ch'+str(i),'smx',1],['ch'+str(i),'sd',j]]
			xm.vmm_mod('vmm.xml',modlist)
			sp.send('vmm','CfgASIC',target)
			time.sleep(0.5)
			threshold = measure_dclevel(scope,scope_channel=scope_chan,meas_id=1,scale=0.05,position=-4.0)
			print '=====channel:',str(i),'trimmed threshold:',threshold
			f.write(" "+str(threshold*1000))
		f.write("\n")
	f.close()



def measure_trimmed_threshold(scope,file_name,**kwargs):

	scope_chan=None
	if kwargs.get('scope_channel') is None: scope_chan=1
	else:   scope_chan=kwargs.get('scope_channel')

	f=open(file_name,'a')
	f.write("======Channel Trimmed Threshold Global ThrDAC=210 =====\n")
	f.write("Channel"+"\t\t\t"+"Threshold (mV)\n")
	for i in range (64):
		modlist=[['global1','scmx',1],['global1','smch',i],['global1','sdt',210],['ch'+str(i),'smx',1]]
		xm.vmm_mod('vmm.xml',modlist)
		sp.send('vmm','CfgASIC',target)
		threshold = measure_dclevel(scope,scope_channel=scope_chan,meas_id=1,scale=0.05,position=-4.0)
		print '=====channel:',str(i),'trimmed threshold:',threshold
		f.write(str(i)+"\t"+str(threshold*1000)+"\n")
	f.close()


def detect_dead_channel(scope,file_name,**kwargs):

	scope_channel=None
	if kwargs.get('scope_channel') is None: scope_channel=1
	else:   scope_channel=kwargs.get('scope_channel')

	f=open(file_name,'a')
	f.write("======Channel Aliveness (PT=50ns,gain=1mV/fC)=====\n")
	f.write("Channel"+"\t"+"Alive?\n")

	for i in range (64):
		modlist=[['global1','scmx',1],['global1','smch',i],['global1','sdp',200+i*10]]
		for k in range (64):	
			if k==i:
				modlist.append(['ch'+str(k),'st',1])
				modlist.append(['ch'+str(k),'sth',1])
				modlist.append(['ch'+str(k),'smx',0])
			else:
				modlist.append(['ch'+str(k),'st',0])
				modlist.append(['ch'+str(k),'sth',0])
				modlist.append(['ch'+str(k),'smx',0])
	
		xm.vmm_mod('vmm.xml',modlist)
		sp.send('vmm','CfgASIC',target)
		is_alive=not (is_deadchannel(scope,scope_channel,meas_id=1))
		print '=====channel:',str(i),'alive?:',is_alive
		f.write(str(i)+"\t"+str(is_alive*1)+"\n")
	f.close()


def noise_level_scan(scope,file_name,**kwargs):

	pt_dict={25:'11',50:'10',100:'01',200:'00'}
	gain_dict={0.5:'000',1.0:'001',3.0:'010',4.5:'011'}

	scope_channel=None
	if kwargs.get('scope_channel') is None: scope_channel=1
	else:   scope_channel=kwargs.get('scope_channel')

	vmm_pt=None
	if kwargs.get('vmm_pt') is None: vmm_pt=50
	else:   vmm_pt=kwargs.get('vmm_pt')

	vmm_gain=None
	if kwargs.get('vmm_gain') is None: vmm_gain=1.0
	else:   vmm_gain=kwargs.get('vmm_gain')


	f=open(file_name,'a')
	f.write("======Channel  (PT="+str(vmm_pt)+"ns,gain="+str(vmm_gain)+"mV/fC)=====\n")
	f.write("Channel"+"\t"+"Noise_RMS(mV)"+"\t"+"ENC"+"\n")

	for i in range (0,64):
		modlist=[['global1','scmx',1],['global1','smch',i],['global1','sdp',0],['global1','spt',pt_dict[vmm_pt]],['global1','sg',gain_dict[vmm_gain]]]
		modlist.append(['ch'+str(i),'st',0])
		modlist.append(['ch'+str(i),'sth',0])
		modlist.append(['ch'+str(i),'smx',0])
	
		xm.vmm_mod('vmm.xml',modlist)
		sp.send('vmm','CfgASIC',target)
		noise_level= measure_noise_rms(scope,scope_channel,meas_id=1)
		#time.sleep(1)
		#filename='CH_'+str(i)+'_PT_'+str(vmm_pt)+'_Gain_'+str(vmm_gain)+".png"
		#scope.save_screen(filename=filename)
		
		enc=((noise_level*1e-15)/vmm_gain)/(1.6e-19)
		print '=====channel:',str(i),'noise_level:',noise_level,"mV","ENC:",enc
		f.write(str(i)+"\t"+str(noise_level)+"\t"+str(enc)+"\n")
	f.close()



def save_csv(scope,**kwargs):
	scope.save_screen(filename=kwargs.get('filename'))
	bashcmd='display '+kwargs.get('filename')
	os.system(bashcmd)


