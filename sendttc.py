#!/usr/bin/env python
import socket
import binascii
from time import sleep
from target_info import *

class SendPacket():

	def __init__(self):
		self.host_ip="192.168.0.16"
		self.host_port=6007
		#self.host_port=7201
		self.dest_ip="192.168.0.1"
		self.dest_port=6008
		self.general_packet_header= binascii.a2b_hex('decafbad') 
		

	def send(self):

		cmd_hex = "00000400"

		cmd = binascii.a2b_hex(cmd_hex)

		print "~~~~~~",cmd
		reg_string = ("000000AF00000000" # FPGA Global RESET (write 0xAA will reset once)
			     #+"000000AB00000004" #TRIGGER MODE (04:EXT. 07:INT.)
			     #+"0000000F00000000" #DAQ_ENA
			     #+"000000CD00000000" #READOUT_ENA
			     #+"000000E000000000" #TTC_STARTBIT (3bits, 0-7)
			     #+"000000E100000000" #Enable TTC Training Pattern For Board 7-0 (8 bits)
			     #+"000000E20000001F" #GATE_Trigger_nBC (in unit of BC, 16 bits, Max 0.2ms)
			     #+"000000E300000000" #Trigger Latency (in unit of BC, 6 bits valid, Max 64BCs)
			     #+"000000E400000000" #SCA_RST
			     #+"000000E500000000" #ECR (Event Counter Reset)
			     +"000000E600000000") #BCR (Bunch Counter Reset)
			     #+"000000E700000000" #VMM_Soft_RST (VMM Soft Reset)
			     #+"000000C100000000" #CKTK Max Number (Unused)
			     #+"000000C200000000" #CKBC Frequency (Unused)
			     #+"000000C30000FFFF" #CKTP_NMAX (16 bits, 0xFFFF==infinite)
			     #+"000000C4000000FF" #CKTP_SKEW (Unused)
			     #+"000000C50000ABCD" #CKTP_PERIOD (Unused)
			     #+"000000C6000000FF" #CKTP_WIDTH (Unused)
			     #+"000000C700000000" #CKBC Max Number (Unused)
			     #+"000000C80000FFFF" #VMM TP SPACING (in unit of BC, 24 bits, minium value: 0xFFFF)
			     #+"000000D0000001FF" #Query Elink Status (8b used, MSB to LSB: elink7-elink0 )
			     #+"000000D100000005" #Elink 3-0 Input Delay (8b/channel, lowerst 5 b effective)
			     #+"000000D201060601") #Elink 7-4 Input Delay (8b/channel,lowerst 5 b effective)
			     #+"000000DC00000000" #Query TTC Lock Status on FEB
			     #+"000000DE00000000")#Query EvID;
	

		payload = binascii.a2b_hex(reg_string)	
	
		message = self.general_packet_header + cmd +  payload 
		mes_vec = [message]

		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sock.bind((self.host_ip, self.host_port))
		
		for i in range(1):
			print "My IP:     " + self.host_ip
			print "My Port:   " + str(self.host_port)
			print "Dest IP:   " + self.dest_ip
			print "Dest Port: " + str(self.dest_port)
			print "Payload:   " + binascii.b2a_hex(mes_vec[i])
			sock.sendto(mes_vec[i], (self.dest_ip, self.dest_port))
			sleep(0.2)


if __name__ == "__main__":
	sp=SendPacket()
	sp.send()

