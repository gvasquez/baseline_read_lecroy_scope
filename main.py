#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import unicode_literals
import os
import psutil

from tekscope_utils import *
from target_info import *
from xmlmodifier import *
from vmm_measlib import *
from vmm_mo_acq import *
from sendpacket import SendPacket

xm=XmlModifier()
sp=SendPacket()


#######################################################################################################
# Simply instructions
# I. introduction to submodules:
# 1) tekscope_untils.py: contains the core class of control commands to tektronix scopes. Includes basic 
#			 functions to adjust vertical/horizontal scales, trigger, get current settings etc.
#
# 2) vmm_mo_acq.py: contains a class of functions to adjust the scope to take amplitude, baseline,
#		    or noise RMS measurement from VMM MO output 
#
# 3) vmm_measlib.py: contains classes, funnctions with several loops to iterate over vmm channels
#		     for vmm_mo measurements. It also saves data into a text file
#
#II. how to use the script:
# 1) Find IP address of scope: the scope needs to be connected the same network where the control PC 
# is connected. Find out the current ip address of the scope by pushing "utility" button on scope buttom  
# then utility page (I/O)-> Ethernet network settings -> Change instrument settings.
# 
# 2) Change IP, file name, scope_chan:
# Change the IP address accordlingly in the main.py and change the file_name as well to save your new 
# measurements. Note default file write setting is "a" which is append. If one want to overwrite the file 
# of the smae name, change "a" to "w" in the f=open(filename,'a'). Check which scope channel is connected 
# with the VMM MO and modifiy "scope_chan" parameter in main.py
# 
# 3) Change board_id, vmm_id:
# One needs to modify the target_info.py to update the FEB board_id and VMM_id paramters.
# Note board_id starts from 0. board_id 0 corresponding to J1 on mSAS_FMC board. vmm_id is a bit mask.
# Therefore, vmm_id = 6 (binary is 000110) refers to the configuration of both second and third VMM
# on the FEB.
#
# 4) execute main.py
# Finally, you should be able to execute the script to perform some basic measurements over all VMM channels
# using main.py. However, one should aware that the calibration or measurement is done by first sending vmm 
# configuration bits to the VMM via KC705 and GBT-SCA and then taking scope measurement (readback results).
# vmm.xml, the vmm configuration file, will be modified during the run. Please save another copy as backup 
# in case the run needs to be force stopped during the loop and the vmm.xml is modified in the midway. 
#=========================================


#==============
# Change Log
#=============


if __name__ == "__main__":

	#====================
	# VMM Calibraton Loop
	#====================
	start=time.time()

	#== setup scope
	#scope=scope('192.168.1.102')
	scope=scope('192.168.0.15')
	scope_chan=1
	#setup_scope(scope,scope_channel=scope_chan)


	#== loops to measure mo output and do calibration via scope
	'''
	meas_id='0'
	board_id='pFEB009'
	vmmid="xxx"
	file_name=meas_id+"_"+board_id+"_VMM3_"+vmmid+".dat"
	f=open(file_name,'a')

	f.write("************************\n")
	f.write("**  VMM ID:"+vmmid+"(x) **\n")
	f.write("************************\n\n\n\n")
  	f.close()		

	#global_threshold_scan(scope,file_name,scope_channel=scope_chan)
	#pulser_scan(scope,file_name,scope_channel=scope_chan)
	#baseline_scan(scope,file_name,scope_channel=scope_chan)
	#channel_threshold_scan(scope,file_name,scope_channel=scope_chan)
	detect_dead_channel(scope,file_name,scope_channel=scope_chan)
	#noise_level_scan(scope,file_name,scope_channel=scope_chan,vmm_pt=25,vmm_gain=3.0)
	'''
	save_csv(scope,filename="test.png")

	'''
	#== the following code illustrate how to save csv files upon trigger
	scope.setup_trigger(channel=2,level=0.3,trigmode='AUTO')
	nEvents=5
	for i in range (nEvents):
		file_name='./Evt_'+str(i)
		scope.save_wfm_upon_trigger(filename=file_name,timeout=2)

	dt=time.time()-start
	print "Time lasped:",dt
	
	#set_optimum_threshold('./vmmcalibration/vmmid_2_opt_channelthr.txt')
	#sp.send('vmm',target)
	#measure_trimmed_threshold(scope,file_name)
	'''
