## Place holder to store asic configuration parameters

from daq_dict import *
from xmlparser import XmlParser
xp = XmlParser()

class target(object):
	def __init__(self,chip):
		self.guest_ip = "192.168.0.1"
		self.guest_port = 6008
		self.board_type = 0 # change to 1 for sFEB
		self.board_id = 1 # corresponding to J1 on mSAS_FMC 
		if (chip=="vmm"):
			self.asic_type = asic_type_dict['VMM']
			self.asic_id = 0b11111111 #configure all 8 VMMs on sFEB
			self.cmd_content=xp.vmm_hexgen("vmm.xml")
		elif (chip=="tds"):
			self.asic_type = asic_type_dict['TDS']
			self.asic_id = 0
			self.cmd_content=xp.tds_hexgen("tds.xml")
		elif (chip=="roc"): 
			self.asic_type = asic_type_dict['ROC']
			self.asic_id = 0
			self.cmd_content=xp.roc_hexgen("fpga_roc.xml")
		elif (chip=="sca_gpio"): 
			self.asic_type = asic_type_dict['SCA']
			self.asic_id = 0
			self.cmd_content=xp.scaGPIO_hexgen("sca_gpio.xml")
		elif (chip=="sca_adc"): 
			self.asic_type = asic_type_dict['SCA']
			self.asic_id = 0
			self.cmd_content=xp.scaADC_hexgen("sca_adc.xml")
		else: 
			self.asic_type = asic_type_dict['VMM']
			self.asic_id = 0
			self.cmd_content=''
