# interpret parameter as BINARY STRING! -- I really mean STRING, not binary number 

class param2bin():
        def __init__(self):

		# TDS Parameter DICT
		self._CKBC_PHASE={
			'0degree':'0011',
			'90degree':'0110',
			'180degree':'1100',
			'270degree':'1001'
		}
		self._STRIP_MATCH_WINDOW={
			'25ns':'0000',
			'31.25ns':'0001',
			'37.5ns':'0011',
			'43.75':'0111',
			'50ns':'1111'
		}
		self._CHAN_DISABLE={'ON':'0','OFF':'1'}
		self._PAD_DLYCOMP={
			'0ns':'00010011',
			'3.125ns':'00000011',
			'6.25ns':'00010110',
			'9.375ns':'00000110',
			'12.5ns':'00011100',
			'15.625ns':'00001100',
			'18.75ns':'00011001',
			'21.875ns':'00001001'
		} #Lower 5 bits Used
		self._BYPASS_PROMT={'BYPASS':'1111','ENABLE':'0000'}
		self._PADTRIG={'ON':'0001','OFF':'0000'}
		self._TEST_ENABLE={'ON':'1','OFF':'0'}
		self._RESET={
			'SER':'00010100',#0x14
			'LOGIC':'00000110',#0x06
			'EPLL':'00100000',#0x20
			'NONE':'00000000',
		}


	def printErr(self):
			print "out-of-range!"

	def parTDS(self,parName,parValue):
		try:
			parNum=int(parValue)
			#print "Num Parameter:",parNum
		except ValueError:
			#print "Text Parameter!",parValue
			pass

		if (parName=="BCID_OFFSET" or parName=="BCID_ROLLOVER"):
			if (parNum<0 or parNum>4095): 
				self.printErr
			else:
				return bin(parNum)[2:].zfill(12)
		elif (parName=="CKBC_PHASE"):
			return self._CKBC_PHASE[parValue]	
		elif (parName=="STRIP_MATCH_WINDOW"):
			return self._STRIP_MATCH_WINDOW[parValue]	
		elif (parName=="CK160_0_PHASE" or parName=="CK160_1_PHASE"):
			if (parNum<0 or parNum>31):
				self.printErr
			else:
				return bin(parNum)[2:].zfill(5)
		elif (parName=="SER_PLL_I"):
			if (parNum<0 or parNum>15):
				self.printErr
			else:
				return bin(parNum)[2:].zfill(4)
		elif (parName=="SER_PLL_R"):
			if (parNum<0 or parNum>3):
				self.printErr
			else:
				return bin(parNum)[2:].zfill(2)
		elif ("CHAN" in parName):
				return self._CHAN_DISABLE[parValue]
		elif ("BANDID" in parName):
			if (parNum<0 or parNum>255):
				self.printErr
			else:
				return bin(parNum)[2:].zfill(9) #0 + 8-bit BANDID		
		elif ("LEADSTRIP" in parName):
			if (parNum<0 or parNum>127):
				self.printErr
			else:
				return bin(parNum)[2:].zfill(7)		
		elif ("DLYCOMP" in parName):
				return self._PAD_DLYCOMP[parValue]
		elif (parName=="RINGBUF_TIMER"):
			if (parNum<0 or parNum>255):
				self.printErr
			else:
				return bin(parNum)[2:].zfill(8)
		elif (parName=="BYPASS_PROMPT"):
				return self._BYPASS_PROMT[parValue]
		elif (parName=="PROMPT_COUNTER"):
			if (parNum<0 or parNum>15):
				self.printErr
			else:
				return bin(parNum)[2:].zfill(4)
		elif (parName=="BYPASS_PADTRIG"):
				return self._PADTRIG[parValue]
		elif (parName=="BYPASS_SCRAMBLER" or 
			parName=="ROUTER_FRMGEN" or 
			parName=="GLOBAL_TEST" or
			parName=="PRBS_GEN"):
			return self._TEST_ENABLE[parValue]
		
		elif (parName=="RESET"):
				return self._RESET[parValue]
		else:
			print "invalid parameter!"
			return False

	def parVMM(self,parName,parValue):
		if (parName in ['sdp','sdt']):
			return bin(int(parValue))[2:].zfill(10)
			print 'pulser/threshold DAC',bin(int(parValue))[2:].zfill(10)
		elif parName == 'smch':
			return bin(int(parValue))[2:].zfill(6)
		elif parName == 'sc6b':
			msb2lsb = bin(int(parValue))[2:].zfill(3)
			return msb2lsb[::-1]  #LSB first
		elif parName == 'sd':
			msb2lsb = bin(int(parValue))[2:].zfill(5)
			return msb2lsb[::-1]  #LSB first
		elif parName == 'sz6b':
			msb2lsb = bin(int(parValue))[2:].zfill(3)
			return msb2lsb[::-1]  #LSB first
		else:	
			return parValue

	def parROC(self,parName,parValue):
		if parName=='vmm_ena_veto':
			return bin(int(parValue))[2:].zfill(8)
			#print 'vmm_ena bit mask',bin(int(parValue))[2:].zfill(8)
		elif parName=='cktp_skew':
			return bin(int(parValue))[2:].zfill(8)
			#print 'cktp vs ckbc phase: tap-',bin(int(parValue))[2:].zfill(8)
		elif (parName in ['cktp_width','trig_window_width']):
			return bin(int(parValue))[2:].zfill(16)
		else:
			print "invalid parameter!"
			return False

	def parSCA_GPIO(self,parName,parValue):
		if (parName in ['gpio_dir','gpio_dout']):
			return parValue
		else:
			print "invalid parameter!"
			return False
			

	def parSCA_ADC(self,parName,parValue):
		try:
			parNum=int(parValue)
			#print "Num Parameter:",parNum
		except ValueError:
			#print "Text Parameter!",parValue
			pass

		if parName=='adc_mux':
			if (parNum<0 or parNum>4095): 
				self.printErr
			else: 
				return bin(int(parValue))[2:].zfill(32)	
		elif parName=='adc_curr':
			return parValue
		else:
			print "invalid parameter!"
			return False



