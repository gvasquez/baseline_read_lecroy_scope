#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import unicode_literals

import vxi11
import time
import re

import os
import psutil

from target_info import *
from xmlmodifier import *
from vmm_measlib import *
from vmm_mo_acq import *
from sendpacket import SendPacket

xm=XmlModifier()
from sendpacket import SendPacket

#scope ip
ip_target = "192.168.10.8"
instr = vxi11.Instrument(ip_target)

def clear_sweeps():
	instrument = vxi11.Instrument(ip_target)
	instrument.write("CLSW")


#Environmental OSC settings


#target="192.168.0.1"

def setting_scope(coupling):
	instr.write("TRMD AUTO")
	instr.write("TDIV 20e-6")
        if(coupling=="DC"):
	    instr.write("TRSE EDGE,SR,C1,HT,OFF")
	    instr.write("C1:VDIV 100MV")
	    instr.write("C1:OFST -400MV")
	    instr.write("C1:CPL D1M")
        elif(coupling=="AC"):
	    instr.write("C1:VDIV 5MV")
	    instr.write("C1:CPL A1M")
        else :
            print "wrong coupling, write DC or AC"
            

def measure_RMS():
	setting_scope("AC")
    
     
    fout = open("rms.txt","w")
	print("ch\t <RMS> \t <sdev(RMS)> \t samples")
	for i in range(0,63):
		clear_sweeps()
		time.sleep(1)
		modlist=[['global1','scmx',1],['global1','smch',i],['global1','stlc',0],['ch'+str(i),'smx',0]]
		xm.vmm_mod('vmm.xml',modlist)
		sp.send('vmm','CfgASIC',target)
		time.sleep(10)
		measure_parameter = instr.ask("PAST? CUST,P1")
	#example of output
	#PAST CUST,P1,RMS,C1,AVG,375.88E-6 V,HIGH,558E-6 V,LAST,402E-6 V,LOW,254E-6 V,SIGMA,34.64E-6 V,SWEEPS,12.792E+3
		value1 = re.search('(?<=AVG,)\w\S+',measure_parameter)
		value2 = re.search('(?<=SIGMA,)\w\S+',measure_parameter)
		value3 = re.search('(?<=SWEEPS,)\w\S+',measure_parameter)
		avg=float(value1.group(0))
		sigma=float(value2.group(0))
		sweeps=int(float(value3.group(0)))
		out ="%d \t %.3e \t %.3e \t %d" %(i,avg,sigma,sweeps)
                fout.write(out)
                print out


def measure_Baseline():
	setting_scope("DC")
    
	instr.write("PACU 2,MEAN,C1")
	fout = open("baseline.txt","w")
	print("ch\t <Baseline> \t <sdev(Baseline)> \t samples")
	for i in range(0,63):
		clear_sweeps()
		modlist=[['global1','scmx',1],['global1','smch',i],['global1','stlc',0],['ch'+str(i),'smx',0]]
		modlist.append(['ch'+str(i),'sm',0])
                #modlist.append(['ch'+str(i),'sth',0])
		xm.vmm_mod('vmm.xml',modlist)
		sp.send('vmm','CfgASIC',target)
		time.sleep(1)
		clear_sweeps()
		time.sleep(10)
		measure_parameter = instr.ask("PAST? CUST,P2")
	#example of output
	#PAST CUST,P1,RMS,C1,AVG,375.88E-6 V,HIGH,558E-6 V,LAST,402E-6 V,LOW,254E-6 V,SIGMA,34.64E-6 V,SWEEPS,12.792E+3
		value1 = re.search('(?<=AVG,)\w\S+',measure_parameter)
		value2 = re.search('(?<=SIGMA,)\w\S+',measure_parameter)
		value3 = re.search('(?<=SWEEPS,)\w\S+',measure_parameter)
		avg=float(value1.group(0))
		sigma=float(value2.group(0))
		sweeps=int(float(value3.group(0)))
		out = "%d \t %.3e \t %.3e \t %d" %(i,avg,sigma,sweeps)
		fout.write(out)
		print out 



#start= time.time()
#print(start)
sp.send('sca','InitSCA',target)
# uncomment RMS or BASELINE function depending if you want to read noise (RMS) or baseline/trheshold (set smx bit to 1) values
measure_Baseline()
#measure_RMS()
#clear_sweeps()
#stop=time.time()
#print(stop-start)






#start measurements


