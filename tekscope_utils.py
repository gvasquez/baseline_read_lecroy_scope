#!/usr/bin/env python

#-------------------------------------------------------
# PROJECT NAME: TEKSCOPE_UNTILS
#
# DATE: 10 April 2017
# AUTHOR: Liang Guan (liang.cuts@gmail.com)
#
# DESCRIPTION:
# This script contains control classes to program and 
# control Tektronik MDO4034-3 scope via Ethernet Interface.
# This is realized by ultizing python-vxi11 library which 
# provides VXI-11 (a TCP/IP protocol specification defined 
# by VXIbus) driver for controlling instruments. 
#
# The high level commands transmitted over the Ethernet
# and used to communicate with the scope follow the RPC 
# protocol (Remote Procedure Call)
#
# This script only includes a subset of commands which are 
# frequently used, such as adjustment of horizontal or vertical
# parameters, taking and save measurements, screenshot etc.
# For more details, see Tektronix MOD 4000 series programmer's 
# manual.
#
# Only control of four analog channles are implemented
#
#
# IMPORTANT NOTE: ALWAYS WAIT AT LEAST 0.5 S BETWEEN QUERIES. 
#		  OTHERWISWE YOU MIGHT GET PREVIOUSLY STORED VALUE!! 
#
#
# ACKNOWLEDGEMENT: saving waveform as csv upon trigger uses script 
#		   from Chris B. posted on tektronix forum  
#--------------------------------------------------------

import sys
import os
import re
import time
import vxi11
from IPy import IP
from struct import unpack

#GLOBAL VARIABLES

CHANNEL = [1,2,3,4]
COUPLING = ['DC','AC','HFRej','LFRej','NOISErej']
SLOPE = ['RISE','FALL','EITHER']

CHANNEL_PARM=['bandwidth','coupling','termination','scale','offset','yunits','invert','deskew','position','label']

MEAS_EGDE = ['RISE','FALL']
MEAS_DIR = ['FORWARDS','BACKWARDS']
MEAS_TYPE=['AMPlitude',
	'AREa',
	'BURst',
	'CARea',
	'CMEan',
	'CRMs',
	'DELay',
	'FALL',
	'FREQuency',
	'HIGH',
	'HITS',
	'LOW',
	'MAXimum',
	'MEAN',
	'MEDian',
	'MINImum',
	'NDUty',
	'NEDGECount',
	'NOVershoot',
	'NPULSECount',
	'NWIdth',
	'PEAKHits',
	'PDUty',
	'PEDGECount',
	'PERIod',
	'PHAse',
	'PK2Pk',
	'POVershoot',
	'PPULSECount',
	'PWIdth',
	'RISe',
	'RMS',
	'SIGMA1',
	'SIGMA2']
MEAS_RSLT_TYPE=['count','value','mean','minimum','maximum','stddev','units']
TRIG_MODE=['EDGe','LOGIC','PULSe','BUS','VIDeo']




class scope(object):

	def __init__(self,IPaddress):
		self.ip = IPaddress
		try:
			IP(self.ip)
		except Exception as e:
			print "Not a Valid IP Address!!",e
		'''
		if len(vxi11.list_devices(self.ip)) is not 0:
                        print "scope present @ IP:",vxi11.list_devices(self.ip)[0] 
			self.s=vxi11.Instrument("TCPIP::"+self.ip+"::INSTR")
			print self.s.ask('*IDN?')
                else:
                        print "scope not found @ IP",self.ip
		'''
		self.s=vxi11.Instrument("TCPIP::"+self.ip+"::INSTR") #quick debug

	# General Write, Read, Query 
	def write(self,cmd):
		nCMD = len(cmd)
		iTrs = divmod(nCMD,8)[0]+1
		for i in range (iTrs):
			if (i!=iTrs-1):
				self.s.write(cmd[i*8:(i+1)*8])
				time.sleep(0.1)
			else:
				self.s.write(cmd[i*8:])
		return True

	# Horizontal Configuration
	def get_horizontal_parameters(self):#return a list of [parameter_name,value]
		
		HORIZ_SET = []
		parm = self.s.ask('HORIzontal?').split(';')
		for i in range (0,len(parm)):
			if (i==0):HORIZ_SET.append(['xx0',parm[i]])
			elif (i==1):HORIZ_SET.append(['acq_sample_rate',parm[i]])
			elif (i==2):HORIZ_SET.append(['delay_precent',parm[i]])
			elif (i==3):HORIZ_SET.append(['sample_rate',parm[i]])
			elif (i==4):HORIZ_SET.append(['time_scale',parm[i]])
			elif (i==5):HORIZ_SET.append(['record_length',parm[i]])
			elif (i==6):HORIZ_SET.append(['delay_mode',parm[i]])
			elif (i==7):HORIZ_SET.append(['delay_time',parm[i]])

		return HORIZ_SET

	def set_horizontal_parameters(self,**kwargs):

		xscale = kwargs.get('xscale')
		delay_mode = kwargs.get('delay_mode')
		delay_time = kwargs.get('delay_time')

		if xscale is None or type(xscale) is not float:
			print "horizontal scale missing or incorrect. needs to be float in seconds"
			print "use default: <100 ns/div>"
			xscale = 100.00E-9 

		if delay_mode is None or \
		   (type(delay_mode) is str and not delay_mode in ['ON','OFF']) or \
		   not type(delay_mode) in [int,str] :
			print "horizontal delay mode missing or incorrect. use default: <OFF>"
			delay_mode = 'OFF'

		if delay_time is None or type(delay_time) is not float:
			print "horizontal delay time missing or incorrect. needs to be float in seconds"
			print "use default: <0.0E-9>"
			delay_time = 0.0E-9 

		cmdlist = ['HORizontal:SCAle '+str(xscale),
			   'HORizontal:DELay:MODe '+delay_mode,
			   'HORizontal:DELay:TIMe '+str(delay_time)]
		self.write(cmdlist)
		return True

	# Vertical Configuration
	def get_channel_parameters(self,channel):

		VERT_SET=[]
		if (type(channel) is not int) or not any(channel==ch for ch in CHANNEL):
			print "channel number has to be 1,2,3 or 4!"
			return VERT_SET

		cmd='CH'+str(channel)+'?'
		parm = self.s.ask(cmd).replace('\"','').split(';')
		for i in range (0,len(parm)):
			if(i==0):VERT_SET.append(['amps_via_volts',parm[i]])
			elif(i==1):VERT_SET.append(['av_ratio',parm[i]]) 
			elif(i==2):VERT_SET.append(['probe_present',parm[i]])
			elif(i==3):VERT_SET.append(['y3',parm[i]])
			elif(i==4):VERT_SET.append(['probe_attenuation',parm[i]])
			elif(i==5):VERT_SET.append(['probe_unit',parm[i]]) #GUESS
			elif(i==6):VERT_SET.append(['y6',parm[i]])
			elif(i==7):VERT_SET.append(['probe_model',parm[i]])
			elif(i==8):VERT_SET.append(['propagation_delay',parm[i]])
			elif(i==9):VERT_SET.append(['y9',parm[i]])
			elif(i==10):VERT_SET.append(['bandwidth',parm[i]])
			elif(i==11):VERT_SET.append(['coupling',parm[i]])
			elif(i==12):VERT_SET.append(['deskew',parm[i]])
			elif(i==13):VERT_SET.append(['offset',parm[i]])
			elif(i==14):VERT_SET.append(['invert',parm[i]])
			elif(i==15):VERT_SET.append(['position',parm[i]])
			elif(i==16):VERT_SET.append(['scale',parm[i]])
			elif(i==17):VERT_SET.append(['unit',parm[i]])
			elif(i==18):VERT_SET.append(['termination',parm[i]])
			elif(i==19):VERT_SET.append(['label',parm[i]])
		return VERT_SET
	
	def set_channel_parameters(self,channel,**kwargs):

		#check channel number validity
		if (type(channel) is not int) or not any(channel==ch for ch in CHANNEL):
			print "channel number has to be 1,2,3 or 4!"
			return False

		# send custom config data
		#-- option = 'all': every paramter has to be specified and is correct. otherwise use default
		#-- option = None or other: only provided parameters sent. return false with sending if any incorrect value detected 
		cmdlist=[]
		parm_value = ''
		for parm in CHANNEL_PARM:
			if kwargs.get('option') == 'all':
				if parm in kwargs.keys() and self.is_valid(parm,kwargs[parm]):
					parm_value=str(kwargs[parm])
				else:
					parm_value=self.set_default(parm)
			else:	
				if parm in kwargs.keys(): 
					if self.is_valid(parm,kwargs[parm]): parm_value=str(kwargs[parm])
					else: return False
				else:
					parm_value = None
			
			if parm_value is not None:cmdlist.append('CH'+str(channel)+':'+parm+' '+parm_value)

		self.write(cmdlist)
		return True


	# Measurement Configuration
	def reset_measurements(self):
		self.s.write('MEASUrement:STATIstics RESET')

	def create_measurement(self,mtype,channel,**kwargs):

		cmdlist=[]
		mid = kwargs.get('meas_id')
		direction = kwargs.get('direction') #
		edge1 = kwargs.get('edge1')	    # delay,phase measurement specific
		edge2 = kwargs.get('edge2')	    #

		# specify measurement slot				
		if mid is None or \
		   type(mid) is not int or \
		   mid > 8 :
			mid = 1 #create default measurement 1
			print "measurement id missing or incorrect (need to be less than 8). By default, create measurement 1."

		# specify measurement edge -- phase,delay meas
		if edge1 is None or \
		   edge2 is None or \
	  	   not any(e.lower()==edge1.lower() for e in MEAS_EGDE) or\
		   not any(e.lower()==edge2.lower() for e in MEAS_EGDE):
			edge1 = 'RISE' 
			edge2 = 'RISE' 
			print "measurement edge missing or incorrect. By default, use <RISE> for edge1 and edge2."

		# specify measurement direction -- phase,delay meas
		if direction is None or \
		   not any(d.lower()==direction.lower() for d in MEAS_DIR):
			direction = 'FORWARDS'
			print "measurement direction missing or incorrect. use default value <FORWARDS> "

		# check channel number validity
		if any(type(channel) is int and ch==channel for ch in CHANNEL) or \
			set(channel).issubset(CHANNEL) and len(channel)==2: 
			pass
		else:
			print "channel input invalid. need to be int or list of int. maximum 2 channels"
			return False

		# define measurement type
		if any(type(mtype) is str and m.lower() ==  mtype.lower() for m in MEAS_TYPE) :
			cmdlist.append('MEASUrement:MEAS'+str(mid)+':TYPe '+mtype)
		else:
			print "unrecognized measurement type. rejected"
			return False

		# define measurement source
		if mtype.lower() in ['delay','phase']  and type(channel) is list:
			cmdlist.append('MEASUrement:MEAS'+str(mid)+":SOUrce1 CH"+str(channel[0]))
			cmdlist.append('MEASUrement:MEAS'+str(mid)+":SOUrce2 CH"+str(channel[1]))
			cmdlist.append('MEASUrement:MEAS'+str(mid)+':DELay:EDGE1 '+edge1)
			cmdlist.append('MEASUrement:MEAS'+str(mid)+':DELay:EDGE2 '+edge2)
			cmdlist.append('MEASUrement:MEAS'+str(mid)+':DELay:DIRection '+direction)
		else:
			cmdlist.append('MEASUrement:MEAS'+str(mid)+":SOUrce1 CH"+str(channel))

		# display on screen
		cmdlist.append('MEASUrement:MEAS'+str(mid)+':STATE ON')
		self.write(cmdlist)
		print "[MESSAGE]:Created Measurement",str(mid),mtype,"(optional)",edge1,edge2,direction
		return True


	def query_measurement_setup(self,meas_id):

		MEAS_SETUP=[]
		if type(meas_id) is not int or meas_id<1 or meas_id>8:
			print "measurement id is not valid. need to be 1-8"
		else:		
			cmd='MEASUrement:MEAS'+str(meas_id)+'?'
			MEAS_SETUP = self.s.ask(cmd).replace('\"','').split(';')
		return MEAS_SETUP


	def query_measurement_results(self,meas_id,**kwargs):

		MEAS_RSLT=[]
		if type(meas_id) is not int or meas_id<1 or meas_id>8:
			print "measurement id is not valid. need to be 1-8"
			return MEAS_RSLT

		rslt_name=[]
		sel=kwargs.get('fetch')
		if sel is None or sel.lower=='all': 
			rslt_name=MEAS_RSLT_TYPE
		elif sel.lower() in MEAS_RSLT_TYPE:
			rslt_name.append(sel)	
		else:
			print "invalid results inqurey!"
			return False	

		for result_type in rslt_name:
			cmd='MEASUrement:MEAS'+str(meas_id)+':'+result_type+'?'
			MEAS_RSLT.append([result_type,self.s.ask(cmd)])
			
		return MEAS_RSLT


	# Trigger Configuration (ONLY EDGE mode implemented now)
	def setup_trigger(self,trigtype=None,**kwargs):

		if trigtype is None:
			trigtype = 'EDGe' # default trigger mode

		channel=kwargs.get('channel')
		trigmode=kwargs.get('trigmode')
		coupling=kwargs.get('coupling')
		slope= kwargs.get('slope')
		level= kwargs.get('level')

		#set default parameters
		if (channel is None) or not any(ch ==channel for ch in CHANNEL): 
			channel = 1
			print "source channel missing or incorrect. use default <CH1>!"
		if (trigmode is None) or not any(md.lower()==trigmode.lower() for md in ['AUTO','NORmal']): 
			trigmode = 'AUTO'
		if (coupling is None) or not any(cp.lower()==coupling.lower() for cp in COUPLING): 
			coupling = 'DC'
			print "channel coupling missing or incorrect. use default <DC>"
		if (slope is None) or not any(s.lower()==slope.lower() for s in SLOPE): 
			slope = 'RISe'
			print "trigger edge missing or incorrect. use default <RISe>"
		if (level is None) or (type(level) is not (float or int)):
			level = 0.1
			print "trigger level should be a float number less than 5.0. use default <0.0V>"

		cmdlist=[]
		if (trigtype.lower() == 'edge'):
				cmdlist.append('SELect:CH'+str(channel)+' ON')
				cmdlist.append('TRIGger:A:TYPe '+trigtype)
				cmdlist.append('TRIGger:A:MODe '+trigmode)
				cmdlist.append('TRIGger:A:EDGE:COUPling '+ coupling)
				cmdlist.append('TRIGger:A:EDGE:SLOpe '+ slope)
				cmdlist.append('TRIGger:A:EDGE:SOUrce CH'+str(channel))
				cmdlist.append('TRIGger:A:LEVel:CH'+str(channel)+' '+str(level))
				self.write(cmdlist)
		else:
			print "invalid trigger mode!"
			return False

		return True

		'''
		TRIGger:A:TYPe LOGIc
		TRIGger:A:LOGIc:INPut:CLOCk:SOUrce NONE
		TRIGger:A:LOGIc:PATtern:WHEn
		TRIGger:A:LOGIc:PATtern:DELTatime
		'''
		
	def detect_error(self):
	
		message = []
		evtreg = self.s.ask("*ESR?")
		message =re.split(r'[,?]+',self.s.ask("ALLEv?").replace('\"',''))
		return message

	# FrontPannel Configuration


	# Data Saving 
	def save_screen (self,**kwargs):

		tstart=time.time()
		filename = kwargs.get('filename')
		if filename is None:		
			print "no filename specified. use default you_should_have_named_it.png"
			filename = 'you_should_have_named_it.png'
			tstart=time.time()
		else:	
			pass			
	
		self.s.write('SAVE:IMAG:FILEF PNG')
		self.s.write('HARDCOPY START')
		raw_data = self.s.read_raw()

		fid = open(filename, 'wb')
		fid.write(raw_data)
		fid.close()
		tstop=time.time()
		telapsed=tstop-tstart
		print "Saving Done",str(telapsed),"seconds elapsed"
		return True

	
	def save_wfm_upon_trigger(self,**kwargs):

		#-- Get input 
		timeout=None
		if kwargs.get('timeout') is None:
			timeout=60 # in seconds
		else:
			timeout=kwargs.get('timeout')

		nEvent=None
		if kwargs.get('nEvent') is None:
			nEvent=1
		else:
			nEvent=kwargs.get('nEvent')

		filename = None
		if kwargs.get('filename') is None:
			print "no filename specified. save waveform file to present directory"
			filename = './Evt_xxx_'
		else:
			filename=kwargs.get('filename')


		#-- Gerneric settings
		self.s.write("VERBOSE OFF")
    
		recLen = self.s.ask("horizontal:recordlength?")
		self.s.write("data:start 1;stop " + recLen + ";:data:encdg rpbinary;:DESE 1;:*ESE 1");
		self.s.write("wfmoutpre:bit_nr 8")
		recLenNumBytes = len(recLen)
		headerLen = 1 + 1 + recLenNumBytes;
		xincr = float(self.s.ask("wfmpre:xincr?"))
		ymult = float(self.s.ask("wfmpre:ymult?"))
		yoff = float(self.s.ask("wfmpre:yoff?"))
		yzero = float(self.s.ask("wfmpre:yzero?"))
		Hscale = float(self.s.ask("HOR:SCA?"))
		HDelay = float(self.s.ask("HORizontal:DELay:TIMe?"))
		HPos = float(self.s.ask("HORIZONTAL:POSITION?"))

	    	#-- Determine which channels are actually on and record to the caputre log
		channelson=range(4)
		for a in channelson:
			channelson[a]=int(self.s.ask("SEL:CH"+ str(a+1) +"?"))
			if channelson[a] == 1:
				print 'Channel ' + str(a+1) + ' is on'

		#-- start single sequence acquisition
		self.s.write('ACQuire:STOPAfter SEQUENCE')
		print "Starting Catpures on " + time.ctime() + '\n'
		
		#-- arm trigger
		end_time= time.time() + timeout
		self.s.write('ACQuire:STATE RUN')
		while time.time() < end_time:
			time.sleep(0.5)
			is_acq_stopped = int(self.s.ask("ACQ:STATE?"))
	        	if is_acq_stopped == 0: # acq stopped
            			triggertime = time.time()
				trig_stat=self.s.ask("TRIGGER:STATE?")
				if trig_stat == 'READY':
                    			print "No events triggered" 
				else:
					print "Event Triggered. Trigger status:",trig_stat
					break
			else:
				print "Waiting for trigger..." 


		#-- save csv data from active channel
		if self.s.ask("TRIGGER:STATE?") != 'READY':
			for a in range(len(channelson)):
				if channelson[a] == 1:
					self.s.write("DATA:SOURCE CH" + str(a+1))
					self.s.write("CURVE?")
					datac = self.s.read_raw()
                    
		                   	# Strip the header
					datac = datac[headerLen:(int(recLen)-1)]
                    			# Convert to byte values
                    			datac = unpack('%sB' % len(datac),datac)
                    			# Convert bytes to voltage values
                    			x = []
                    			y = []
                    			for i in range(0,len(datac)):
						x.append((i-(len(datac)*(HPos/100)))* xincr + HDelay)
						y.append(((datac[i]-yoff) * ymult) + yzero)
					#save curve data in .csv format
					fw = open(filename + '_CH_' + str(a+1)+'.csv', 'w')
					fw.write("s,Volts\n")
                    			for i in range(0,len(datac)):
                 			       fw.write(str(x[i]) + ',' + str(y[i])+ '\n')
                    			fw.close()

	

	def set_default (self,parm):

		value=''
		if parm=='ampsviavolts': value=':ENABLE ON'
		elif parm=='bandwidth': value='350.00E6'
		elif parm=='coupling': value='DC'
		elif parm=='termination': value='50.0'
		elif parm=='scale': value='1.0'
		elif parm=='offset': value='0.0'
		elif parm=='yunits': value='\"V\"'
		elif parm=='invert': value='off'
		elif parm=='deskew': value='0.0'
		elif parm=='position': value='0.0'
		elif parm=='label': value='\"LABEL\"'
		else: pass

		return value 

	def is_valid (self,parm,value):
		if (parm=='ampsviavolts' and value in ['ON','OFF']) or \
		   (parm=='bandwidth' and (type(value) is float) and value>0.0 and value<350.00E6) or \
		   (parm=='coupling' and value in COUPLING) or \
		   (parm=='termination' and value in [1.0E6,50.0]) or \
		   (parm=='scale' and type(value) is float) or \
		   (parm=='offset' and type(value) is float)  or \
		   (parm=='yunits' and type(value) is str) or \
		   (parm=='invert' and (type(value) is string) and value.lower() in ['on','off']) or \
		   (parm=='deskew' and type(value) is float) or \
		   (parm=='position' and (type(value) is float) and value>-5.0 and value<5.0) or \
		   (parm=='label' and type(value) is str):
			return True
		else:
			print '!!parameter:',parm,'specified as',value,'is incorrect!!'
			return False

