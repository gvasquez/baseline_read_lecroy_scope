# Lecroy Waverunner control
python script to control Lecroy Waverunner scope through VXi11 protocol.

The script lecroy.py contains two functions to read either Baseline or RMS values. 
Scope settings can be change according your needs.

The comunication through tcp is done through <vxi11>  python module. 
To install please use pip:

```
pip install vxi11
```

The ip address from Scope must be within the same network of the minidaq, therefore 192.168.0.x.
The scope ip can be set on lecroy.py

The rest of python code correspond to the ATLAS FIRMWARE from Liang Guang (https://gitlab.cern.ch/lguan/ATLAS_NSW_sTGC_miniDAQ_Firmware) which enable the comunication with the minidaq1 or 2.


To use just run:
```
python lecroy.py
```
